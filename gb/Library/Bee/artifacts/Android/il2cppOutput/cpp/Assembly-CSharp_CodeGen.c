﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void MedKitInteractable::OnTriggerEnter(UnityEngine.Collider)
extern void MedKitInteractable_OnTriggerEnter_m4A77A4EF1AFEBE6BF64CBE769D14095254D87F3A (void);
// 0x00000002 System.Void MedKitInteractable::OnTriggerExit(UnityEngine.Collider)
extern void MedKitInteractable_OnTriggerExit_m6CA73FBF138C349E03A54628964D316032ACAE17 (void);
// 0x00000003 System.Void MedKitInteractable::Update()
extern void MedKitInteractable_Update_m417520D25EE24C58320562FD6D86D0D7F0F3AF42 (void);
// 0x00000004 System.Void MedKitInteractable::.ctor()
extern void MedKitInteractable__ctor_mBF95737419E7C8CBAC320B20349E36F5EEAD4D13 (void);
// 0x00000005 System.Void PropManager::Awake()
extern void PropManager_Awake_mFB233F9D68F677E0CFE70A10329E44E12F1422A9 (void);
// 0x00000006 System.Void PropManager::ItemInteractPickup(UnityEngine.GameObject)
extern void PropManager_ItemInteractPickup_m7526E425628A3C65628196D49D6E81B4ED21091C (void);
// 0x00000007 System.Void PropManager::ItemInteractPack(UnityEngine.GameObject)
extern void PropManager_ItemInteractPack_m5902FFF435B5A367F59B3B7E309BE7952736B55B (void);
// 0x00000008 System.Void PropManager::.ctor()
extern void PropManager__ctor_m59681C7C427D8A97A2D9884C9BFE3EB098682BD3 (void);
// 0x00000009 System.Void BFVR.InputModule.BFVRHandTrackingController::Start()
extern void BFVRHandTrackingController_Start_mC0213C05D4982341ED9A4E16D5A41D8B89857630 (void);
// 0x0000000A System.Void BFVR.InputModule.BFVRHandTrackingController::Update()
extern void BFVRHandTrackingController_Update_m56BE05D5D1CB72F8614B27ED63A413D416213D4A (void);
// 0x0000000B System.Void BFVR.InputModule.BFVRHandTrackingController::InitializeXRSubsystem()
extern void BFVRHandTrackingController_InitializeXRSubsystem_m7012EB1077713D1A83C3E4A2E7F941F04F6367BA (void);
// 0x0000000C System.Void BFVR.InputModule.BFVRHandTrackingController::UpdateHandTracking()
extern void BFVRHandTrackingController_UpdateHandTracking_m81A5D8AC929FD1615BC8459CD44F8981B7FD73BF (void);
// 0x0000000D System.Void BFVR.InputModule.BFVRHandTrackingController::.ctor()
extern void BFVRHandTrackingController__ctor_mF7366C9BB472DDA6CDC609EF997A46308A68B9E5 (void);
static Il2CppMethodPointer s_methodPointers[13] = 
{
	MedKitInteractable_OnTriggerEnter_m4A77A4EF1AFEBE6BF64CBE769D14095254D87F3A,
	MedKitInteractable_OnTriggerExit_m6CA73FBF138C349E03A54628964D316032ACAE17,
	MedKitInteractable_Update_m417520D25EE24C58320562FD6D86D0D7F0F3AF42,
	MedKitInteractable__ctor_mBF95737419E7C8CBAC320B20349E36F5EEAD4D13,
	PropManager_Awake_mFB233F9D68F677E0CFE70A10329E44E12F1422A9,
	PropManager_ItemInteractPickup_m7526E425628A3C65628196D49D6E81B4ED21091C,
	PropManager_ItemInteractPack_m5902FFF435B5A367F59B3B7E309BE7952736B55B,
	PropManager__ctor_m59681C7C427D8A97A2D9884C9BFE3EB098682BD3,
	BFVRHandTrackingController_Start_mC0213C05D4982341ED9A4E16D5A41D8B89857630,
	BFVRHandTrackingController_Update_m56BE05D5D1CB72F8614B27ED63A413D416213D4A,
	BFVRHandTrackingController_InitializeXRSubsystem_m7012EB1077713D1A83C3E4A2E7F941F04F6367BA,
	BFVRHandTrackingController_UpdateHandTracking_m81A5D8AC929FD1615BC8459CD44F8981B7FD73BF,
	BFVRHandTrackingController__ctor_mF7366C9BB472DDA6CDC609EF997A46308A68B9E5,
};
static const int32_t s_InvokerIndices[13] = 
{
	3499,
	3499,
	4402,
	4402,
	4402,
	3499,
	3499,
	4402,
	4402,
	4402,
	4402,
	4402,
	4402,
};
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	13,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
