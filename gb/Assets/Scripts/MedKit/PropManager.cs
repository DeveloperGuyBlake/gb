using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PropManager : MonoBehaviour
{
    public static PropManager propManager;

    public GameObject instructionsText1;//TextMesh containing instructions for step 1
    public GameObject instructionsText2;//TextMesh containing instructions for step 2

    public bool itemHeld = false;

    int packedItemCount = 0;//how many items have been packed into the medkit.

    void Awake()
    {
        propManager = this;
    }

    public void ItemInteractPickup()//user pick up an item from the table
    {
        instructionsText1.SetActive(false);
        instructionsText2.SetActive(true);
        itemHeld = true;
    }

    public void ItemInteractPack()//user packs the held item into the med kit
    {
        instructionsText1.SetActive(true);
        instructionsText2.SetActive(false);
        itemHeld = false;
        packedItemCount++;

        //TODO: Play a sound indicating the item was packed into the kit
        
        if (packedItemCount == 15) StartCoroutine("SceneTransition");
    }

    //public void ItemInteractReturn(GameObject c) //user returns item to its original position
    //{
    //    instructionsText1.SetActive(true);
    //    instructionsText2.SetActive(false);
    //}

    IEnumerator SceneTransition()
    {
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(1);
    }
}
