using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.XR.OpenXR.Input;
using UnityEngine.XR;

public class MedKitInteractable : MonoBehaviour
{
    public static InputFeatureUsage<float> trigger;

    public GameObject outlineMesh;
    public GameObject textMesh;//floating text item that should appear when this item is highlighted
    public GameObject packedItem;//The item that represent's this item's location in the medkit (used for object-swapping).

    bool isHighlighted = false;//can this item be picked up?
    GameObject hitObject;

    void OnTriggerEnter(Collider hit)
    {
        if (hit.gameObject.layer == LayerMask.NameToLayer("HandTrigger"))
        {
            isHighlighted = true;
            outlineMesh.SetActive(true);
            textMesh.SetActive(true);
            hitObject = hit.gameObject;
        }

        else if (hit.gameObject.layer == LayerMask.NameToLayer("MedKitTrigger"))
        {
            packedItem.SetActive(true);
            PropManager.propManager.ItemInteractPack();
            this.transform.SetParent(null);
            this.gameObject.SetActive(false);
        }
    }

    void OnTriggerExit(Collider hit)
    {
        if (hit.gameObject.layer == LayerMask.NameToLayer("HandTrigger"))
        {
            textMesh.SetActive(false);
            outlineMesh.SetActive(false);
            isHighlighted = false;
        }
    }

    void Update()
    {
        if (isHighlighted && !PropManager.propManager.itemHeld && Input.GetButtonDown("Fire1"))//When we click 'pick up' while this interactable is highlighted, pick up the item
        {
            this.transform.SetParent(hitObject.transform);
            outlineMesh.SetActive(false);
            textMesh.SetActive(false);
            PropManager.propManager.ItemInteractPickup();
        }
    }

    //TODO: IF PLACE OBJECT BACK ON TABLE, RE-ENABLE COLLIDER
}
